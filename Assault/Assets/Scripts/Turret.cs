﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float bulletSpeed = 30.0f;

    // Used to determine facing of the player's view
    public GameObject headObject;

    public GameObject target;

    private float NumBulletsToFire = 0f;

    private float bulletsPerSecond = 0.5f;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 directionToTarget = (target.transform.position - headObject.transform.position).normalized;
        headObject.transform.rotation = Quaternion.LookRotation(directionToTarget);

        if (NumBulletsToFire < 1f)
        {
            NumBulletsToFire += bulletsPerSecond * Time.deltaTime;
        }

        int layerMask = LayerMask.GetMask("Enemy", "Enemy projectile");
        layerMask = ~layerMask;

        RaycastHit hitInfo;
        bool rayHitSomething = Physics.Raycast(headObject.transform.position, directionToTarget, out hitInfo, Mathf.Infinity, layerMask);
        bool canSeeTarget = (rayHitSomething && hitInfo.collider.CompareTag("Player"));


        if (canSeeTarget && NumBulletsToFire >= 1f)
        {
            GameObject bullet = Instantiate(bulletPrefab, headObject.transform.position, headObject.transform.rotation);

            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.velocity = bulletSpeed * headObject.transform.forward;
            }

            NumBulletsToFire -= 1f;
        }
    }
}
