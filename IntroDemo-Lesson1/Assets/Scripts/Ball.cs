﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public PlayerController player1;
    public PlayerController player2;

    // Needs to move
    // Needs to collide with paddles
    // Needs to collide with walls
    // Needs to interact with goals

    public Vector3 velocity = new Vector3(100, 100, 0);

    public Transform startingPosition;

    public GameObject ballPrefab;

    private float splitDelayTimer = -1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (splitDelayTimer > 0f)
            splitDelayTimer -= Time.deltaTime;

        transform.position += velocity * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            velocity.x = -velocity.x;

            // Create another ball
            if (ballPrefab != null && splitDelayTimer <= 0f)
            {
                for (int i = 0; i < 2; ++i)
                {
                    GameObject newBall = Instantiate(ballPrefab, transform.position, Quaternion.identity);
                    Ball ball = newBall.GetComponent<Ball>();
                    ball.player1 = player1;
                    ball.player2 = player2;
                    ball.velocity = new Vector3(velocity.x, Random.Range(-100.0f, 100.0f), 0f);
                    ball.startingPosition = startingPosition;
                    ball.splitDelayTimer = 1.0f;
                    ball.ballPrefab = ballPrefab;
                }
                
                // Destroy(ball.gameObject);
            }
        }
        else if (other.CompareTag("Wall"))
        {
            velocity.y = -velocity.y;
        }
        else if (other.CompareTag("Goal1"))
        {
            player2.score += 1;
            player2.UpdateScoreText();
            transform.position = startingPosition.position;
        }
        else if (other.CompareTag("Goal2"))
        {
            player1.score += 1;
            player1.UpdateScoreText();
            transform.position = startingPosition.position;
        }
    }
}
