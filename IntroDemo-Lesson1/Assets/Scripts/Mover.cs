﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public Vector3 amount;

    /*private Vector3 _position;
    public void SetPosition(Vector3 newPosition)
    {
        _position = newPosition;
    }

    public Vector3 GetPosition()
    {
        return _position;
    }*/

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Move 1 meter per second in the x-dimension
        transform.position += amount * Time.deltaTime;

        //transform.position.x = 5;
        //Debug.Log("x position: " + transform.position.x);
        //transform.position = new Vector3(5, transform.position.y, transform.position.z);
    }
}
