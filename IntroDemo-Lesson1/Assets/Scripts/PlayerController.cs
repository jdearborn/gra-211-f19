﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int playerID;

    public Text scoreText;
    public int score = 0;
    public float speed = 100f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Quit();
        }

        float vert = 0.0f;
        if(playerID == 1)
        {
            if (Input.GetKey(KeyCode.W))
                vert = speed;
            if (Input.GetKey(KeyCode.S))
                vert = -speed;
        }
        else if(playerID == 2)
        {
            if (Input.GetKey(KeyCode.UpArrow))
                vert = speed;
            if (Input.GetKey(KeyCode.DownArrow))
                vert = -speed;
        }

        float topWallY = 437;
        float bottomWallY = 18;

        bool hitWall = false;
        if (transform.position.y + vert * Time.deltaTime > topWallY)
            hitWall = true;
        if (transform.position.y + vert * Time.deltaTime < bottomWallY)
            hitWall = true;

        if (hitWall)
            vert = 0f;

        transform.position += new Vector3(0f, vert, 0f) * Time.deltaTime;
    }

    public void UpdateScoreText()
    {
        scoreText.text = "P" + playerID.ToString() + "'s Score: " + score.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Wall"))
        {
            Debug.Log("You hit a wall!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Goal"))
        {
            Debug.Log("You win!");
        }
    }
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
