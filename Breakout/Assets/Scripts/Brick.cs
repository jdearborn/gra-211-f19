﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public int hitpoints = 1;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.numBricks++;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        hitpoints--;
        if (hitpoints <= 0)
        {
            ScoreKeeper.instance.score += 5;
            Destroy(gameObject);
            GameManager.numBricks--;

            if (GameManager.numBricks == 0)
            {
                SceneHelper.instance.LoadNextLevel();
            }
        }
    }
}
