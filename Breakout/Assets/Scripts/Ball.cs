﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void StartMoving()
    {
        float speed = 15.0f;

        // Get unit vector in desired direction
        Vector3 direction = new Vector3(Random.Range(-1.0f, 1.0f), 0f, 1.0f).normalized;
        //direction.Normalize();

        rb.velocity = speed * direction;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
