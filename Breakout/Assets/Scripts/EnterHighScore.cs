﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class EnterHighScore : MonoBehaviour
{
    public Text playerScoreText;
    public InputField nameInput;

    void Start()
    {
        playerScoreText.text = ScoreKeeper.instance.score.ToString();
    }
    

    public void Done()
    {
        // Read the player's name
        string name = nameInput.text;

        // Combine name and score into a string
        string combined = name + "\t" + ScoreKeeper.instance.score.ToString();

        // Save that string in high score table
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/high scores.txt", true);
        writer.WriteLine();
        writer.Write(combined);
        writer.Close();

        // TODO: Sort these by putting them into an object: {name, score} and making a List of objects like that.  Sort them by the score.
        // Another way: Open the high score file as "read/write" and insert this new entry into the spot it belongs (higher scores above, lower beneath).

    }
}
