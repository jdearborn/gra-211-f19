﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10.0f;

    public Ball ball;

    private Rigidbody rb;

    public BigPowerupEffect currentPowerup;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxis("Horizontal");
        
        // Without rigidbody component (or with kinematic enabled):
        //transform.position += new Vector3(horiz, 0f, 0f) * speed * Time.deltaTime;
        
        rb.velocity = new Vector3(horiz, 0f, 0f) * speed;

        if (ball != null)
        {
            ball.GetComponent<Rigidbody>().velocity = rb.velocity;
        }
        

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Time.timeScale = 0.2f;
        }
        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            Time.timeScale = 1.0f;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            ball.StartMoving();
            ball = null;
        }

        if(currentPowerup != null)
        {
            currentPowerup.Update(this, Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Ball"))
        {
            Vector3 forceDir = (collision.collider.transform.position - transform.position).normalized;
            Rigidbody rb = collision.collider.GetComponent<Rigidbody>();

            float impulsePower = 1.0f;
            rb.AddForce(impulsePower * forceDir, ForceMode.Impulse);
        }
    }
}
