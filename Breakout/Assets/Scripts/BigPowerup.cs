﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigPowerup : MonoBehaviour
{
    public float speed = 1.0f;
    public float effectStrength = 0.8f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.back * speed * Time.deltaTime;

        transform.rotation = Quaternion.AngleAxis(-360 * Time.deltaTime, Vector3.right) * transform.rotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.currentPowerup = new BigPowerupEffect();
            player.currentPowerup.ApplyEffect(player);

            Destroy(gameObject);
        }
    }
}
