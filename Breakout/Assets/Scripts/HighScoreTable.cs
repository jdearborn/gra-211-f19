﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class HighScoreTable : MonoBehaviour
{
    public Text[] entries;

    // Start is called before the first frame update
    void Start()
    {
        // Erase all scores
        for(int i = 0; i < entries.Length; ++i)
        {
            entries[i].text = "";
        }

        Debug.Log(Application.persistentDataPath);

        // Read scores from a file
        // REMINDER: Talk about exceptions
        StreamReader reader = new StreamReader(Application.persistentDataPath + "/high scores.txt");

        int currentEntry = 0;
        while(reader.Peek() > -1)
        {
            string line = reader.ReadLine();

            if (currentEntry >= entries.Length)
                break;

            entries[currentEntry].text = line;

            currentEntry++;
        }

        reader.Close();
    }
    
}
