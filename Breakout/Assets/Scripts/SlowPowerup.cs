﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowPowerup : MonoBehaviour
{
    public float speed = 1.0f;
    public float effectStrength = 0.8f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.back * speed * Time.deltaTime;

        transform.rotation = Quaternion.AngleAxis(-360*Time.deltaTime, Vector3.right) * transform.rotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            // Slow down all balls
            Ball[] balls = FindObjectsOfType<Ball>();
            for(int i = 0; i < balls.Length; ++i)
            {
                balls[i].rb.velocity *= 1/effectStrength;
            }

            Destroy(gameObject);
        }
    }
}
