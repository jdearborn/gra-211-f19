﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject redBrick;
    public GameObject blueBrick;

    public static int numBricks = 0;

    // Start is called before the first frame update
    void Start()
    {
        CreateBricks();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateBricks()
    {
        numBricks = 0;

        //int n = 0;
        for (int x = -3; x < 4; ++x)
        {
            for (int z = 0; z > -1; --z)
            {
                Vector3 position = new Vector3(transform.position.x + x * 5, transform.position.y, transform.position.z + z * 3);

                GameObject brickType;
                //if (x > 0)
                //if(n % 2 == 0)

                if ((x + 3) % 2 == z % 2)
                    brickType = redBrick;
                else
                    brickType = blueBrick;

                Instantiate(brickType, position, Quaternion.identity);
                //n++;
            }
        }
    }
}
