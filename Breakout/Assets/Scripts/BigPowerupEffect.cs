﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigPowerupEffect
{
    public float effectDuration = 8.0f;

    public void ApplyEffect(PlayerController player)
    {
        Vector3 newScale = player.transform.localScale;
        newScale.x *= 1.6f;
        player.transform.localScale = newScale;
    }

    public void RemoveEffect(PlayerController player)
    {
        Vector3 newScale = player.transform.localScale;
        newScale.x /= 1.6f;
        player.transform.localScale = newScale;

        player.currentPowerup = null;
    }

    public void Update(PlayerController player, float dt)
    {
        if (effectDuration > 0.0f)
        {
            effectDuration -= dt;
            if (effectDuration <= 0.0f)
            {
                RemoveEffect(player);
            }
        }
    }
}
